import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:luxemall/model/product.dart';
import 'package:luxemall/util/color.dart';
import 'package:luxemall/util/endpoint.dart';
import 'package:luxemall/util/network.dart';
import 'package:luxemall/viewmodel/products_viewmodel.dart';

class ProductDetailPage extends StatefulWidget {
  int id;

  ProductDetailPage({this.id});

  @override
  _ProductDetailPageState createState() => _ProductDetailPageState();
}

class _ProductDetailPageState extends State<ProductDetailPage> {
  ProductsViewModel productsViewModel = ProductsViewModel();
  Product product;
  int quantity = 1;
  bool error = false;

  @override
  void initState() {
    // TODO: implement initState
    getProduct();
    super.initState();
  }

  getProduct(){
    productsViewModel.getSingleProduct(widget.id).then((value) {
      if (value != null) {
        setState(() {
          product = value;
        });
      } else {
        print('value null : $value');
        setState(() {
          error = true;
          print('error nih : ${error}');
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: error
              ? Container(
            child: Center(
              child: Column(mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.error,color: AppColor.GOLD,size: 100,),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("We are sorry, we have run into an issue"),
                  ),
                  FlatButton(child: Text('Retry',style: TextStyle(color: AppColor.WHITE),),onPressed: (){
                    setState(() {
                      error = false;
                      getProduct();
                    });
                  },color: AppColor.GREY,)
                ],
              ),
            ),
          )
              : product == null
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : Container(
                      width: double.infinity,
                      color: Colors.white,
                      child: Column(
                        children: [
                          Expanded(
                            child: Container(
                              height: MediaQuery.of(context).size.height / 2,
                              child: Stack(
                                children: [
                                  Container(
                                    width: double.infinity,
                                    color: Colors.white,
                                    alignment: Alignment.center,
                                    child: Image.network(
                                      product.image,
                                    ),
                                  ),
                                  Positioned(
                                    left: 1,
                                    top: 1,
                                    child: IconButton(
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                      icon: Icon(Icons.arrow_back_ios),
                                      color: AppColor.GREY,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: EdgeInsets.all(24),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    product.title,
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                        color: AppColor.GREY),
                                  ),
                                  Text(
                                    toBeginningOfSentenceCase(
                                        product.description),
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: Colors.grey),
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    width: double.infinity,
                                    padding: EdgeInsets.symmetric(vertical: 16),
                                    child: Text(
                                      'FREE DELIVERY ON ORDERS ABOVE \$250.0',
                                      style: TextStyle(color: AppColor.GOLD),
                                    ),
                                    decoration: BoxDecoration(
                                        border: Border(
                                            top: BorderSide(
                                                color: AppColor.GOLD),
                                            bottom: BorderSide(
                                                color: AppColor.GOLD))),
                                  ),
                                  Row(
                                    children: [
                                      FlatButton(
                                        child: Icon(
                                          Icons.remove,
                                          color: AppColor.GREY,
                                        ),
                                        onPressed: () {
                                          if (quantity > 1) {
                                            setState(() {
                                              quantity -= 1;
                                            });
                                          }
                                        },
                                        color: Colors.transparent,
                                        shape: RoundedRectangleBorder(
                                            side: BorderSide(
                                                color: AppColor.GREY),
                                            borderRadius:
                                                BorderRadius.circular(4)),
                                      ),
                                      Text(
                                        quantity.toString(),
                                        style: TextStyle(
                                            fontSize: 24,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      FlatButton(
                                        child: Icon(
                                          Icons.add,
                                          color: AppColor.GREY,
                                        ),
                                        onPressed: () {
                                          setState(() {
                                            quantity += 1;
                                          });
                                        },
                                        color: Colors.transparent,
                                        shape: RoundedRectangleBorder(
                                            side: BorderSide(
                                                color: AppColor.GREY),
                                            borderRadius:
                                                BorderRadius.circular(4)),
                                      ),
                                    ],
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                  ),
                                  FlatButton(
                                    padding: EdgeInsets.all(8),
                                    minWidth: double.infinity,
                                    shape: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(4),
                                        borderSide: BorderSide.none),
                                    child: Text(
                                      'BUY \$ ${product.price * quantity}',
                                      style: TextStyle(
                                          fontSize: 18,
                                          color: AppColor.WHITE,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    color: AppColor.GREY,
                                    onPressed: () {
                                      Network.getApi(Endpoint.CARTS);
                                      Fluttertoast.showToast(
                                          msg: "Item add to cart",
                                          toastLength: Toast.LENGTH_SHORT,
                                          gravity: ToastGravity.CENTER,backgroundColor: AppColor.GOLD
                                      );
                                    },
                                  ),
                                  Text(
                                    'HAVE QUESTIONS?',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: AppColor.GREY,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Column(
                                          children: [
                                            Container(
                                              child: Icon(
                                                Icons.question_answer,
                                                color: AppColor.GREY,
                                              ),
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                    color: AppColor.GREY),
                                                borderRadius:
                                                    BorderRadius.circular(100),
                                              ),
                                              padding: EdgeInsets.all(8),
                                            ),
                                            Text(
                                              'Chat with one of our agents',
                                              textAlign: TextAlign.center,
                                            )
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        child: Column(
                                          children: [
                                            Container(
                                              child: Icon(
                                                Icons.call,
                                                color: AppColor.GREY,
                                              ),
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                    color: AppColor.GREY),
                                                borderRadius:
                                                    BorderRadius.circular(100),
                                              ),
                                              padding: EdgeInsets.all(8),
                                            ),
                                            Text(
                                              'Call our agents',
                                              textAlign: TextAlign.center,
                                            )
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        child: Column(
                                          children: [
                                            Container(
                                              child: Icon(
                                                Icons.assistant_photo,
                                                color: AppColor.GREY,
                                              ),
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                    color: AppColor.GREY),
                                                borderRadius:
                                                    BorderRadius.circular(100),
                                              ),
                                              padding: EdgeInsets.all(8),
                                            ),
                                            Text(
                                              'Visit our help section',
                                              textAlign: TextAlign.center,
                                            )
                                          ],
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    )),
    );
  }
}
