import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:luxemall/util/color.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        child: Column(
          children: [
            Image.asset('assets/new.jpg'),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 16,vertical: 32),
               color: Colors.black,
              child: Column(
                children: [
                  Text(
                    'DEAR BELOVED CUSTOMER',
                    style: TextStyle(
                        fontSize: 24,
                        color: Colors.white),
                  ),
                  SizedBox(height: 8,),
                  Text(
                    'To all our athletes, teammates and friends across the country. due to the current situation. please expect some delays in your orders as we try our best to deliver them to you. we will continue to monitor and adjust according to developments. while putting our families and communities first.',
                    style: TextStyle(
                        color: Colors.white),textAlign: TextAlign.center,
                  )
                ],
              ),
            ),
            Image.asset('assets/womens.png'),
            Image.asset('assets/accessories.jpg'),
            Image.asset('assets/mens.jpg'),
          ],
        ),
      ),
    );
  }
}
