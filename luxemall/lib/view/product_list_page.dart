import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:luxemall/model/category.dart';
import 'package:luxemall/model/product.dart';
import 'package:luxemall/util/color.dart';
import 'package:luxemall/util/endpoint.dart';
import 'package:luxemall/view/product_detail_page.dart';
import 'package:luxemall/viewmodel/products_viewmodel.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ProductList extends StatefulWidget {
  ProductList({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ProductListState createState() => _ProductListState();
}

class _ProductListState extends State<ProductList> {
  ProductsViewModel productsViewModel = ProductsViewModel();
  List<Product> products;
  List<Category> categories = [
    Category(name: 'jewelery', selected: false),
    Category(name: 'men clothing', selected: false),
    Category(name: 'electronics', selected: false),
    Category(name: 'women clothing', selected: false),
  ];
  bool selected = false;
  String sortBy = 'asc';
  String selectCategory = 'All Products';
  int itemToShow = 4;
  bool error = false;

  @override
  void initState() {
    // TODO: implement initState
    getAllProducts();
    super.initState();
  }

  String _groupValue = 'none';

  Future getAllProducts() async{
    await productsViewModel.getAllProducts(items: itemToShow).then((value) {
      if(value!=null){
        setState(() {
          itemToShow +=itemToShow;
        });
      }else{
        print('value null : $value');
        setState(() {
          error = true;
          print('error nih : ${error}');
        });
      }

    });
  }

  @override
  Widget build(BuildContext context) {
    var mediaQueryData = MediaQuery.of(context);
    final double widthScreen = mediaQueryData.size.width;
    final double appBarHeight = kToolbarHeight;
    final double paddingBottom = mediaQueryData.padding.bottom;
    final double heightScreen =
        mediaQueryData.size.height / 1.5 - paddingBottom - appBarHeight;

    RefreshController _refreshController =
        RefreshController(initialRefresh: false);

    void _onLoading() async {
      // monitor network fetch
      await Future.delayed(Duration(milliseconds: 1000));
      // if failed,use loadFailed(),if no data return,use LoadNodata()
      getAllProducts();
      if(mounted)
        setState(() {

        });
      _refreshController.loadComplete();

    }

    Widget _myRadioButton({String title, String value, Function onChanged}) {
      return SizedBox(
        height: 30,
        width: double.infinity,
        child: RadioListTile(
          dense: true,
          value: value,
          groupValue: _groupValue,
          onChanged: onChanged,
          title: Text(title),
        ),
      );
    }

    void _modalBottomSheetMenu() {
      showModalBottomSheet(
          context: context,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          backgroundColor: Colors.white,
          builder: (builder) {
            String itemSelect;
            return StatefulBuilder(
              builder: (BuildContext context, StateSetter changeInside) {
                return Container(
                    height: 400,
                    decoration: new BoxDecoration(
                        color: Colors.white,
                        borderRadius: new BorderRadius.only(
                            topLeft: const Radius.circular(10.0),
                            topRight: const Radius.circular(10.0))),
                    child: new Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Text(
                            'REFINE RESULTS',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 20),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 16),
                          alignment: Alignment.centerLeft,
                          child: Text(
                            'CATEGORIES',
                            style: TextStyle(
                                fontSize: 16,
                                color: AppColor.GREY,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        GridView.builder(
                            shrinkWrap: true,
                            padding: EdgeInsets.all(16),
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 3,
                                    childAspectRatio: 3 / 1,
                                    mainAxisSpacing: 8,
                                    crossAxisSpacing: 8),
                            itemCount: categories.length,
                            itemBuilder: (context, index) {
                              return InkWell(
                                onTap: () {
                                  changeInside(() {
                                    {
                                      categories.forEach((element) {
                                        element.selected = false;
                                      });
                                      categories[index].selected = true;
                                      itemSelect = categories[index].name;
                                    }
                                  });
                                },
                                child: Container(
                                  alignment: Alignment.center,
                                  child: Text(
                                    toBeginningOfSentenceCase(
                                      categories[index].name,
                                    ),
                                    style: TextStyle(
                                        color: categories[index].selected
                                            ? AppColor.WHITE
                                            : AppColor.GREY),
                                  ),
                                  decoration: BoxDecoration(
                                      color: categories[index].selected
                                          ? AppColor.GOLD
                                          : AppColor.WHITE,
                                      border: Border.all(
                                          color: categories[index].selected
                                              ? AppColor.GOLD
                                              : Colors.grey),
                                      borderRadius: BorderRadius.circular(8)),
                                ),
                              );
                            }),
                        Container(
                          padding: EdgeInsets.only(left: 16),
                          alignment: Alignment.centerLeft,
                          child: Text(
                            'SORT BY',
                            style: TextStyle(
                                fontSize: 16,
                                color: AppColor.GREY,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Column(
                          children: <Widget>[
                            _myRadioButton(
                              title: "Ascending",
                              value: 'asc',
                              onChanged: (newValue) => changeInside(
                                  () => _groupValue = newValue.toString()),
                            ),
                            _myRadioButton(
                              title: "Descending",
                              value: 'desc',
                              onChanged: (newValue) => changeInside(
                                  () => _groupValue = newValue.toString()),
                            ),
                          ],
                        ),
                        Spacer(),
                        FlatButton(
                          padding: EdgeInsets.all(24),
                          minWidth: double.infinity,
                          shape: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(4),
                              borderSide: BorderSide.none),
                          child: Text(
                            'APPLY FILTERS',
                            style: TextStyle(
                                fontSize: 18,
                                color: AppColor.WHITE,
                                fontWeight: FontWeight.bold),
                          ),
                          color: AppColor.GREY,
                          onPressed: () {
                            productsViewModel
                                .getProductByCategory(
                                    itemSelect + Endpoint.SORT + _groupValue)
                                .then((value) {
                              categories.forEach((element) {
                                element.selected = false;
                              });
                              setState(() {
                                sortBy = _groupValue;
                                _groupValue = 'none';
                              });

                              Navigator.pop(context);
                            });
                          },
                        ),
                      ],
                    ));
              },
            );
          });
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              Icons.search,
              color: Colors.black,
            ),
            Flexible(
                child: TextField(
              decoration: InputDecoration(
                contentPadding: EdgeInsets.only(left: 16),
                hintText: 'Find Products...',
                border: InputBorder.none,
              ),
            )),
          ],
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.shopping_bag,
              color: AppColor.GOLD,
            ),
            onPressed: () {},
          )
        ],
      ),
      body: error
          ? Container(
        child: Center(
          child: Column(mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(Icons.error,color: AppColor.GOLD,size: 100,),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("We are sorry, we have run into an issue"),
              ),
              FlatButton(child: Text('Retry',style: TextStyle(color: AppColor.WHITE),),onPressed: (){
                setState(() {
                  itemToShow = 4;
                  error = false;
                  getAllProducts();
                });
              },color: AppColor.GREY,)
            ],
          ),
        ),
      ):SmartRefresher(
        onLoading: _onLoading,
        enablePullUp: true,
        enablePullDown: false,
        controller: _refreshController,
        footer: CustomFooter(
          builder: (BuildContext context, LoadStatus mode) {
            Widget body;
            if(mode==LoadStatus.idle){
              body =  Text("pull up load");
            }
            else if(mode==LoadStatus.loading){
              body =  CupertinoActivityIndicator();
            }
            else if(mode == LoadStatus.failed){
              body = Text("Load Failed!Click retry!");
            }
            else if(mode == LoadStatus.canLoading){
              body = Text("release to load more");
            }
            else{
              body = Text("No more Data");
            }
            return Container(
              height: 55.0,
              child: Center(child:body),
            );
          },
        ),
        child: SingleChildScrollView(
          padding: EdgeInsets.all(16),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    selectCategory.toUpperCase(),
                    style: TextStyle(color: AppColor.GREY),
                  ),
                  IconButton(
                    icon: Icon(
                      Icons.filter_list,
                      color: AppColor.GREY,
                    ),
                    onPressed: () {
//                     print('click jewelry');
//                     productsViewModel.getProductByCategory('jewelery');
                      _modalBottomSheetMenu();
                    },
                  )
                ],
              ),
              Divider(
                thickness: 1,
                color: AppColor.GOLD,
              ),
              StreamBuilder<List<Product>>(
                stream: productsViewModel.observableProduct,
                builder: (_, snapshot) {
                  if (!snapshot.hasData) {
                    return Container(height: MediaQuery.of(context).size.height/1.5,
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    );
                  }
                  products = List.from(snapshot.data);
                  if (sortBy == 'asc') {
                    print('ini ASCENIDING');
                    print(sortBy);

                    products.sort((b, a) => -a.title.compareTo(b.title));
                  } else {
                    print('ini DESCENIDING');
                    print(sortBy);

                    products.sort((b, a) => a.title.compareTo(b.title));
                  }

                  return GridView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        mainAxisSpacing: 32,
                        crossAxisSpacing: 16,
                        childAspectRatio: widthScreen / heightScreen),
                    itemCount: products.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ProductDetailPage(
                                        id: products[index].id,
                                      )));
                        },
                        child: Container(
                          child: Column(
                            children: [
                              Expanded(
                                child: Container(
                                  child: Image.network(
                                    products[index].image,
                                  ),
                                ),
                                flex: 4,
                              ),
                              Expanded(
                                child: Container(
                                  width: double.infinity,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(top: 8.0),
                                        child: Text(
                                          products[index].title.toUpperCase(),
                                          style: TextStyle(fontSize: 16),
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ),
                                      Spacer(),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 8.0),
                                        child: Text(
                                          toBeginningOfSentenceCase(
                                              products[index].category),
                                          style:
                                              TextStyle(color: AppColor.GOLD),
                                        ),
                                      ),
                                      Text(
                                        "\$ ${products[index].price.toString()}",
                                        style: TextStyle(color: Colors.black),
                                      ),
                                    ],
                                  ),
                                ),
                                flex: 2,
                              )
                            ],
                          ),
                        ),
                      );
                    },
                  );
                },
              )
            ],
          ),
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
