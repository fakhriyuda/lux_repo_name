import 'package:flutter/material.dart';

class AppColor{
  static const Color GOLD = Color(0xFFC2912E);
  static const Color WHITE = Color(0xFFFFFFFF);
  static const Color GREY = Color(0xFF585858);

}