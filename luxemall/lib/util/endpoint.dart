class Endpoint{
  static const String _BASEURL = 'https://fakestoreapi.com/';
  static const String ALL_PRODUCTS = _BASEURL + 'products?limit=';
  static const String SINGLE_PRODUCT = _BASEURL + 'products/';
  static const String CARTS = _BASEURL + 'carts';
  static const String PRODUCT_BY_CATEGORY = _BASEURL + 'products/category/';
  static const String SORT = '/?sort=';




}