import 'package:dio/dio.dart';

class Network {
  static Future<dynamic> postApi(String url, FormData formData) async {
    try {
      Response rest = await Dio().post(url, data: formData);
      print('postApi:$url');
      print(rest.data);
      return rest.data;
    } on DioError catch (e) {
      var error = Map<String, dynamic>();
      error['code'] = e.response.statusCode;
      error['message'] = e.response.statusMessage;
      return error;
    }
  }

  static Future<dynamic> postApiWithHeaders(String url, FormData body, Map<String, dynamic> header) async {
    try {
      Response restValue = await Dio().post(url, data: body, options: Options(headers: header));
      print('postApiWithHeaders:$url');
      print('restValue:$restValue');
      return restValue.data;
    } on DioError catch (e) {
      var error = Map<String, dynamic>();
      error['code'] = e.response.statusCode;
      error['message'] = e.response.statusMessage;
      return error;
    }
  }

  static Future<dynamic> getApi(String url) async {
    try {
      Response restGet = await Dio().get(url);
      print('getApi:$url');
      return restGet.data;
    } on DioError catch (e) {
      var error = Map<String, dynamic>();
      error['err'] = e.error;
      return null;
    }
  }
}
