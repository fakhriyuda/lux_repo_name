import 'dart:collection';

import 'package:luxemall/model/product.dart';
import 'package:luxemall/util/endpoint.dart';
import 'package:luxemall/util/network.dart';
import 'package:rxdart/rxdart.dart';

class ProductsViewModel {
  var controllerProducts = BehaviorSubject<List<Product>>();
  Sink<List<Product>> get sinkProduct => controllerProducts.sink;
  Observable<List<Product>> get observableProduct => controllerProducts.stream;

  Future getAllProducts({int items}) async {
    var resp = await Network.getApi(Endpoint.ALL_PRODUCTS+items.toString());
    if(resp!=null){
      List lp = resp;
      UnmodifiableListView<Product> prods = UnmodifiableListView(lp.map((e) => Product.fromJson(e)).toList());
      print('ini products : $prods');
      sinkProduct.add(prods);
      return prods;
    }else{
      print('null nih : $resp');
      return null;

    }

  }
  Future getCarts() async {
    var resp = await Network.getApi(Endpoint.CARTS);
    if(resp!=null){
      List lp = resp;
      print('ini carts : $lp');
      return lp;
    }else{
      print('null nih : $resp');
      return null;

    }

  }

  Future<Product> getSingleProduct(int id) async {
    var resp = await Network.getApi(Endpoint.SINGLE_PRODUCT+id.toString());
    if(resp!=null){
      print(resp);
      Product cons = Product.fromJson(resp);
      return cons;
    }
    print('resp : $resp');
    return resp;
  }
  Future<Product> getProductByCategory(String category) async {
    print('masuk nih.. ${Endpoint.PRODUCT_BY_CATEGORY+category}');
    var resp = await Network.getApi(Endpoint.PRODUCT_BY_CATEGORY+category);
    if(resp!=null){
      List lp = resp;
      UnmodifiableListView<Product> prods = UnmodifiableListView(lp.map((e) => Product.fromJson(e)).toList());
      print('ini products : $prods');
      sinkProduct.add(prods);
    }
    print('null nih : $resp');
  }
}
