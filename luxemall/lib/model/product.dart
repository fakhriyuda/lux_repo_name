import 'package:json_annotation/json_annotation.dart';

part 'product.g.dart';

@JsonSerializable(nullable: true, explicitToJson: true)
class Product {
  int id;
  String title;
  double price;
  String description;
  String category;
  String image;
//  @JsonKey(defaultValue: false)
//  bool isSelected = false;

  Product(
      {this.image,this.price,this.title,this.category,this.description,this.id});

  factory Product.fromJson(Map<String, dynamic> json) =>
      _$ProductFromJson(json);

  Map<String, dynamic> toJson() => _$ProductToJson(this);
}
